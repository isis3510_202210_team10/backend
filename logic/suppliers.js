const Joi = require("@hapi/joi");

// Scheme to validate each piece of data held by a supplier
const schema = Joi.object().keys({
  name: Joi.string().min(3).max(50),
  email: Joi.string().email().required(),
  address: Joi.string().min(5).max(100).required(),
  phone: Joi.string().min(8).max(13).required(),
  companyName: Joi.string().min(5).max(25)
});

// Validation of a supplier's input data 
validation = (data) => {
  const result = schema.validate(data);
  if (result.error != null) {
    return [false, result.error.details[0].message];
  } else {
    return [true, "Supplier created successfully"];
  }
};

module.exports = validation;