const Joi = require("@hapi/joi");

// Scheme to validate each piece of data held by a supplier
const schema = Joi.object().keys({
  name: Joi.string().min(3).max(20).required(),
  description: Joi.string().min(4).max(50).required(),
  type: Joi.string().valid('FOOD', 'CLEANING', 'TECHNOLOGY', 'CLOTING', 'STATIONERY').required(),
  image: Joi.string().uri().required(),
  expirationDate: Joi.date().min("now").iso().required(),
  priceSupplier: Joi.number().greater(0).required(),
  stock: Joi.string().min(0).required(),
  shopId: Joi.string().required()
});

// Validation of a supplier's input data 
validation = (data) => {
  const result = schema.validate(data);
  if (result.error != null) {
    return [false, result.error.details[0].message];
  } else {
    return [true, "Shop product created successfully"];
  }
};

module.exports = validation;
