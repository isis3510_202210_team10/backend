const Joi = require("@hapi/joi");

const schema = Joi.object().keys({
  name: Joi.string().min(3).max(20).required(),
  email: Joi.string().email().required(),
  address: Joi.string().min(5).max(100).required(),
  phone: Joi.string().min(8).max(13).required(),
  type: Joi.string().valid('FOOD', 'HARDWARE', 'CLOTHING', 'STATIONERY')
});

validation = (data) => {
  const result = schema.validate(data);
  if (result.error != null) {
    return [false, result.error.details[0].message];
  } else {
    return [true, "Shop created successfully"];
  }
};

module.exports = validation;
