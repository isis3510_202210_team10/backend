const Joi = require("@hapi/joi").extend(require('@joi/date'));

const date = new Date();
date.setHours(0, 0, 0, 0);

const schema = Joi.object().keys({
  name: Joi.string().min(3).max(20).required(),
  description: Joi.string().min(4).max(200).required(),
  type: Joi.string().valid('FOOD', 'CLEANING', 'TECHNOLOGY', 'CLOTHING','STATIONERY').required(),
  image: Joi.string().min(8).max(100).required(),
  expirationDate: Joi.date().format('YYYY-MM-DD').min(date).required(),
  priceSupplier: Joi.number().required(),
  stock: Joi.number().integer().required(),
  supplierId: Joi.string().required()
});

validation = (data) => {
  const result = schema.validate(data);
  if (result.error != null) {
    return [false, result.error.details[0].message];
  } else {
    return [true, "ProductSupplier created successfully"];
  }
};

module.exports = validation;
