const Joi = require("@hapi/joi");

const schema = Joi.object().keys({
  date: Joi.date()
  .min('1-1-2000')
  .max('now')
  .required(),
  message: Joi.string()
  .min(20)
  .max(200)
  .required(),  
  type: Joi.string()
  .valid('ORDER', 'PRODUCT')
  .required(),
  userId: Joi.string().required()
});

validation = (data) => {
  const result = schema.validate(data);
  if (result.error != null) {
    return [false, result.error.details[0].message];
  } else {
    return [true, "Alert created successfully"];
  }
};

module.exports = validation;
