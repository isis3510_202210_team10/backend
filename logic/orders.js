const Joi = require("@hapi/joi").extend(require('@joi/date'));

const date = new Date();
date.setHours(0, 0, 0, 0);

const schema = Joi.object().keys({
  initialDate: Joi.date().format('YYYY-MM-DD').min(date).required(),
  arrivalDate: Joi.date().format('YYYY-MM-DD').min(date).required(),
  address: Joi.string().min(5).max(100).required(),
  status: Joi.string().valid('ON_REVISION', 'ACCEPTED', 'DECLINED', 'SHIPPING', 'COMPLETED').required(),
  total: Joi.number().positive().required(),
  shopId: Joi.string().required(),
  supplierId: Joi.string().required()
});

validation = (data) => {
  const result = schema.validate(data);
  if (result.error != null) {
    return [false, result.error.details[0].message];
  } else {
    return [true, "Order created successfully"];
  }
};

module.exports = validation;