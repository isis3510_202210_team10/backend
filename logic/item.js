const Joi = require("@hapi/joi");

const schema = Joi.object().keys({
  price: Joi.number()
  .min(0)
  .max(10000000)
  .required(),
  quantity: Joi.number()
  .min(0)
  .max(200)
  .required(),  
  orderId: Joi.string().required(),
  productSupplierId: Joi.string().required()
});

validation = (data) => {
  const result = schema.validate(data);
  if (result.error != null) {
    return [false, result.error.details[0].message];
  } else {
    return [true, "Item created successfully"];
  }
};

module.exports = validation;
