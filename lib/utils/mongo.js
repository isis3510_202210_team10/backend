const { MongoClient, ServerApiVersion } = require('mongodb');
const env = require('dotenv').config()
const uri = `mongodb+srv://TeamFrijolito:${process.env.PASSWORD_MONGO_DB}@inshopcluster.ghsia.mongodb.net/${process.env.DATABASE_NAME_MONGO_DB}?retryWrites=true&w=majority`;

function MongoUtils() {
  const mu = {};

  // Esta función retorna una nueva conexión a MongoDB.
  // Tenga presente que es una promesa que deberá ser resuelta.
  mu.conn = () => {
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true, serverApi: ServerApiVersion.v1 });
    return client.connect();
  };
  return mu;
}

module.exports = MongoUtils();
