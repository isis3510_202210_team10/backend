var express = require("express");
var router = express.Router();
var shopsLogic = require("../logic/shops");
var [
  getShops,
  getShopID,
  insertShop,
  updateShop,
  deleteShop,
  getOrdersFromShopId,
  getShopByEmail,
  getProductsFromShopId,
  getAlertsFromShopId,
] = require("../controllers/shops");

/**
 * GET all shops
 */
router.get("/", async function (req, res, next) {
  const shops = await getShops();
  res.send(shops);
});

/**
 * GET shop by ID
 */
router.get("/:id", async function (req, res, next) {
  try{
    const shop = await getShopID(req.params.id);
    if(shop!=null){
      res.send(shop);
    }
    else{
      res.status(404).send("Shop didn't found by ID")
    }
  }
  catch(err){
    res.status(404).send("Shop didn't found by ID")
  }
});

/**
 * GET shop by EMAIL
 */
 router.get("/email/:email", async function (req, res, next) {
  try{
    const shop = await getShopByEmail(req.params.email);
    res.send(shop);
  }
  catch(err){
    res.status(404).send("Shop didn't found by EMAIL")
  }
});

/**
 * POST shop
 */
router.post("/", async function (req, res, next) {
  if (shopsLogic(req.body)[0]) {
    const shop = await insertShop(req.body);
    res.send(shop);
  } else {
    res.status(406).send(shopsLogic(req.body)[1]);
  }
});

/**
 * UPDATE shop
 */
router.put("/:id", async function (req, res, next) {
  try{
    if (shopsLogic(req.body)[0]) {
      const shop = await updateShop(req.params.id, req.body);
      res.send(shop);
    } else {
      res.status(406).send(shopsLogic(req.body)[1]);
    }
  }
  catch(err){
    res.status(404).send("Shop didn't found by ID")
  }
});

/**
 * DELETE shop
 */
router.delete("/:id", async function (req, res, next) {
  try{
    const shop = await deleteShop(req.params.id);
    res.send(shop);
  }
  catch(err){
    res.status(404).send("Shop didn't found by ID")
  }
});

/**
 * GET orders from shop ID
 */
 router.get("/:id/orders", async function (req, res, next) {
  try{
    const order = await getOrdersFromShopId(req.params.id);
    res.send(order);
  }
  catch(err){
    res.status(404).send("Orders didn't found by shop ID")
  }
});

/**
 * GET products from shop ID
 */
 router.get("/:id/shopProducts", async function (req, res, next) {
  try{
    const order = await getProductsFromShopId(req.params.id);
    res.send(order);
  }
  catch(err){
    res.status(404).send("Products didn't found by shop ID")
  }
});

/**
 * GET alerts from shop ID
 */
 router.get("/:id/alerts", async function (req, res, next) {
  try{
    const order = await getAlertsFromShopId(req.params.id);
    res.send(order);
  }
  catch(err){
    res.status(404).send("Alerts didn't found by shop ID")
  }
});

module.exports = router;
