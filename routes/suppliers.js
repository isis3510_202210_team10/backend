var express = require("express");
var router = express.Router();
var suppliersLogic = require("../logic/suppliers");
var [
  getSuppliers,
  getSupplierID,
  insertSupplier,
  updateSupplier,
  deleteSupplier,
  getOrdersFromSupplierId,
  getSupplierByEmail,
  getAlertsFromSupplierId,
] = require("../controllers/suppliers");

/**
 * GET all suppliers
 */
router.get("/", async function (req, res, next) {
  const suppliers = await getSuppliers();
  res.send(suppliers);
});

/**
 * GET supplier by ID
 */
router.get("/:id", async function (req, res, next) {
  try{
    const supplier = await getSupplierID(req.params.id);
    if(supplier!=null){
      res.send(supplier);
    }
    else{
      res.status(404).send("Supplier didn't found by ID")
    }
  }
  catch(err){
    res.status(404).send("Supplier didn't found by ID")
  }
});

/**
 * GET supplier by EMAIL
 */
 router.get("/email/:email", async function (req, res, next) {
  try{
    const supplier = await getSupplierByEmail(req.params.email);
    console.log(supplier)
    res.send(supplier);
  }
  catch(err){
    res.status(404).send("Supplier didn't found by EMAIL")
  }
});

/**
 * POST supplier
 */
router.post("/", async function (req, res, next) {
  if (suppliersLogic(req.body)[0]) {
    const supplier = await insertSupplier(req.body);
    res.send(supplier);
  } else {
    res.status(406).send(suppliersLogic(req.body)[1]);
  }
});

/**
 * UPDATE supplier
 */
router.put("/:id", async function (req, res, next) {
  try{
    if (suppliersLogic(req.body)[0]) {
      const supplier = await updateSupplier(req.params.id, req.body);
      res.send(supplier);
    } else {
      res.status(406).send(suppliersLogic(req.body)[1]);
    }
  }
  catch(err){
    res.status(404).send("Supplier didn't found by ID")
  }
});

/**
 * DELETE supplier
 */
router.delete("/:id", async function (req, res, next) {
  try{
    const supplier = await deleteSupplier(req.params.id);
    res.send(supplier);
  }
  catch(err){
    res.status(404).send("Supplier didn't found by ID")
  }
});

/**
 * GET orders from supplier ID
 */
 router.get("/:id/orders", async function (req, res, next) {
  try{
    const order = await getOrdersFromSupplierId(req.params.id);
    res.send(order);
  }
  catch(err){
    res.status(404).send("Orders didn't found by supplier ID")
  }
});

/**
 * GET alerts from supplier ID
 */
 router.get("/:id/alerts", async function (req, res, next) {
  try{
    const order = await getAlertsFromSupplierId(req.params.id);
    res.send(order);
  }
  catch(err){
    res.status(404).send("Alerts didn't found by supplier ID")
  }
});

module.exports = router;