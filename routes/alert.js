var express = require("express");
var router = express.Router();
var alertLogic = require("../logic/alert");
var [
  getAlerts,
  getAlertID,
  insertAlert,
  updateAlert,
  deleteAlert,
] = require("../controllers/alert");

/**
 * GET all alerts
 */
router.get("/", async function (req, res, next) {
  const alerts = await getAlerts();
  res.send(alerts);
});

/**
 * GET item by ID
 */
router.get("/:id", async function (req, res, next) {
  try{
    const alert = await getAlertID(req.params.id);
    res.send(alert);
  }
  catch(err){
    res.status(404).send("Item didn't found by ID")
  }
});

/**
 * POST Item
 */
router.post("/", async function (req, res, next) {
  if (alertLogic(req.body)[0]) {
    const item = await insertAlert(req.body);
    res.send(item);
  } else {
    res.status(406).send(alertLogic(req.body)[1]);
  }
});

/**
 * UPDATE item
 */
router.put("/:id", async function (req, res, next) {
  try{
    if (alertLogic(req.body)[0]) {
      const item = await updateAlert(req.params.id, req.body);
      res.send(item);
    } else {
      res.status(406).send(alertLogic(req.body)[1]);
    }
  }
  catch(err){
    res.status(404).send("Item didn't found by ID")
  }
});

/**
 * DELETE item
 */
router.delete("/:id", async function (req, res, next) {
  try{
    const item = await deleteAlert(req.params.id);
    res.send(item);
  }
  catch(err){
    res.status(404).send("Item didn't found by ID")
  }
});

module.exports = router;
