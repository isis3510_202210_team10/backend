var express = require("express");
var router = express.Router();
var ProductSuppliersLogic = require("../logic/productSupplier");
var [
  getProductSuppliers,
  getProductSupplierID,
  insertProductSupplier,
  updateProductSupplier,
  deleteProductSupplier,
] = require("../controllers/productSupplier");

/**
 * GET all ProductSuppliers
 */
router.get("/", async function (req, res, next) {
  const ProductSuppliers = await getProductSuppliers();
  res.send(ProductSuppliers);
});

/**
 * GET ProductSupplier by ID
 */
router.get("/:id", async function (req, res, next) {
  try{
    const ProductSupplier = await getProductSupplierID(req.params.id);
    res.send(ProductSupplier);
  }
  catch(err){
    res.status(404).send("ProductSupplier didn't found by ID")
  }
});

/**
 * POST ProductSupplier
 */
router.post("/", async function (req, res, next) {
  if (ProductSuppliersLogic(req.body)[0]) {
    const ProductSupplier = await insertProductSupplier(req.body);
    res.send(ProductSupplier);
  } else {
    res.status(406).send(ProductSuppliersLogic(req.body)[1]);
  }
});

/**
 * UPDATE ProductSupplier
 */
router.put("/:id", async function (req, res, next) {
  try{
    if (ProductSuppliersLogic(req.body)[0]) {
      const ProductSupplier = await updateProductSupplier(req.params.id, req.body);
      res.send(ProductSupplier);
    } else {
      res.status(406).send(ProductSuppliersLogic(req.body)[1]);
    }
  }
  catch(err){
    res.status(404).send("ProductSupplier didn't found by ID")
  }
});

/**
 * DELETE ProductSupplier
 */
router.delete("/:id", async function (req, res, next) {
  try{
    const ProductSupplier = await deleteProductSupplier(req.params.id);
    res.send(ProductSupplier);
  }
  catch(err){
    res.status(404).send("ProductSupplier didn't found by ID")
  }
});

module.exports = router;
