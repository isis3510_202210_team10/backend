var express = require("express");
var router = express.Router();
var shopProductsLogic = require("../logic/shopProducts");
var [
    getShopProducts, 
    getProductShopID, 
    insertProductShop, 
    updateShopProduct, 
    deleteProductShop
] = require("../controllers/shopProducts");

/**
 * GET all shop products
 */
router.get("/", async function (req, res, next) {
  const shopProducts = await getShopProducts();
  res.send(shopProducts);
});

/**
 * GET shop product by ID
 */
router.get("/:id", async function (req, res, next) {
  try{
    const shopProduct = await getProductShopID(req.params.id);
    res.send(shopProduct);
  }
  catch(err){
    res.status(404).send("Shop product didn't found by ID")
  }
});

/**
 * POST shop product
 */
router.post("/", async function (req, res, next) {
  if (shopProductsLogic(req.body)[0]) {
    const shopProduct = await insertProductShop(req.body);
    res.send(shopProduct);
  } else {
    res.status(406).send(shopProductsLogic(req.body)[1]);
  }
});

/**
 * UPDATE shop product
 */
router.put("/:id", async function (req, res, next) {
  try{
    if (shopProductsLogic(req.body)[0]) {
      const shopProduct = await updateShopProduct(req.params.id, req.body);
      res.send(shopProduct);
    } else {
      res.status(406).send(shopProductsLogic(req.body)[1]);
    }
  }
  catch(err){
    res.status(404).send("Shop product didn't found by ID")
  }
});

/**
 * DELETE shop product
 */
router.delete("/:id", async function (req, res, next) {
  try{
    const shopProduct = await deleteProductShop(req.params.id);
    res.send(shopProduct);
  }
  catch(err){
    res.status(404).send("Shop product didn't found by ID")
  }
});

module.exports = router;