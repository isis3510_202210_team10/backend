var express = require("express");
var router = express.Router();
var ordersLogic = require("../logic/orders");
var [
  getOrders,
  getOrderID,
  insertOrder,
  updateOrder,
  deleteOrder,
  getItemsFromOrderId
] = require("../controllers/orders");

/**
 * GET all orders
 */
router.get("/", async function (req, res, next) {
  const orders = await getOrders();
  res.send(orders);
});

/**
 * GET order by ID
 */
router.get("/:id", async function (req, res, next) {
  try{
    const order = await getOrderID(req.params.id);
    res.send(order);
  }
  catch(err){
    res.status(404).send("Order didn't found by ID")
  }
});

/**
 * POST order
 */
router.post("/", async function (req, res, next) {
  if (ordersLogic(req.body)[0]) {
    const order = await insertOrder(req.body);
    res.send(order);
  } else {
    res.status(406).send(ordersLogic(req.body)[1]);
  }
});

/**
 * UPDATE order
 */
router.put("/:id", async function (req, res, next) {
  try{
    if (ordersLogic(req.body)[0]) {
      const order = await updateOrder(req.params.id, req.body);
      res.send(order);
    } else {
      res.status(406).send(ordersLogic(req.body)[1]);
    }
  }
  catch(err){
    res.status(404).send("Order didn't found by ID")
  }
});

/**
 * DELETE order
 */
router.delete("/:id", async function (req, res, next) {
  try{
    const order = await deleteOrder(req.params.id);
    res.send(order);
  }
  catch(err){
    res.status(404).send("Order didn't found by ID")
  }
});

/**
 * GET items from order ID
 */
 router.get("/:id/items", async function (req, res, next) {
  try{
    const order = await getItemsFromOrderId(req.params.id);
    res.send(order);
  }
  catch(err){
    res.status(404).send("Items didn't found by order ID")
  }
});

module.exports = router;