var express = require("express");
var router = express.Router();
var itemsLogic = require("../logic/item");
var [
  getItems,
  getItemID,
  insertItem,
  updateItem,
  deleteItem,
] = require("../controllers/item");

/**
 * GET all items
 */
router.get("/", async function (req, res, next) {
  const items = await getItems();
  res.send(items);
});

/**
 * GET item by ID
 */
router.get("/:id", async function (req, res, next) {
  try{
    const items = await getItemID(req.params.id);
    res.send(items);
  }
  catch(err){
    res.status(404).send("Item didn't found by ID")
  }
});

/**
 * POST Item
 */
router.post("/", async function (req, res, next) {
  if (itemsLogic(req.body)[0]) {
    const item = await insertItem(req.body);
    res.send(item);
  } else {
    res.status(406).send(itemsLogic(req.body)[1]);
  }
});

/**
 * UPDATE item
 */
router.put("/:id", async function (req, res, next) {
  try{
    if (itemsLogic(req.body)[0]) {
      const item = await updateItem(req.params.id, req.body);
      res.send(item);
    } else {
      res.status(406).send(itemsLogic(req.body)[1]);
    }
  }
  catch(err){
    res.status(404).send("Item didn't found by ID")
  }
});

/**
 * DELETE item
 */
router.delete("/:id", async function (req, res, next) {
  try{
    const item = await deleteItem(req.params.id);
    res.send(item);
  }
  catch(err){
    res.status(404).send("Item didn't found by ID")
  }
});

module.exports = router;
