const mdbconn = require("../lib/utils/mongo");
const {ObjectId} = require('mongodb');
const env = require("dotenv").config();

function getAlerts() {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("alerts")
      .find({})
      .toArray();
  });
}

function getAlertID(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("alerts")
      .findOne({"_id" : new ObjectId(id)});
  });
}

function insertAlert(alert) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("alerts")
      .insertOne(alert); // Si no se provee un ID, este será generado automáticamente
  });
}

function updateAlert(id, data) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("alerts" )
      .updateOne(
        { _id: new ObjectId(id) }, // Filtro al documento que queremos modificar
        { $set: data } // El cambio que se quiere realizar
      );
  });
}

function deleteAlert(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("alerts" )
      .deleteOne({ _id: new ObjectId(id)});
  });
}

module.exports = [getAlerts, getAlertID, insertAlert, updateAlert, deleteAlert];
