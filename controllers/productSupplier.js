const mdbconn = require("../lib/utils/mongo");
const {ObjectId} = require('mongodb');
const env = require("dotenv").config();

function getProductSuppliers() {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("productSuppliers")
      .find({})
      .toArray();
  });
}

function getProductSupplierID(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("productSuppliers")
      .findOne({"_id" : new ObjectId(id)});
  });
}

function insertProductSupplier(ProductSupplier) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("productSuppliers")
      .insertOne(ProductSupplier); // Si no se provee un ID, este será generado automáticamente
  });
}

function updateProductSupplier(id, data) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("productSuppliers")
      .updateOne(
        { _id: new ObjectId(id) }, // Filtro al documento que queremos modificar
        { $set: data } // El cambio que se quiere realizar
      );
  });
}

function deleteProductSupplier(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("productSuppliers")
      .deleteOne({ _id: new ObjectId(id)});
  });
}

module.exports = [getProductSuppliers, getProductSupplierID, insertProductSupplier, updateProductSupplier, deleteProductSupplier];