const mdbconn = require("../lib/utils/mongo");
const {ObjectId} = require('mongodb');
const env = require("dotenv").config();

// Get the shop products registered in the database
function getShopProducts() {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shopProducts")
      .find({})
      .toArray();
  });
}

// Get one shop product registered in the database
function getProductShopID(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shopProducts")
      .findOne({"_id" : new ObjectId(id)});
  });
}

// Inserting a new shop product to the database
function insertProductShop(shopProduct) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shopProducts")
      .insertOne(shopProduct); // If an ID is not provided, it will be automatically generated.
  });
}

// Updating an existing shop product in the database
function updateShopProduct(id, data) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shopProducts")
      .updateOne(
        { _id: new ObjectId(id) }, // Filter to the document we want to modify
        { $set: data } // The change that needs to be made
      );
  });
}

// Delete an existing shop product in the database
function deleteProductShop(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shopProducts")
      .deleteOne({ _id: new ObjectId(id)});
  });
}

module.exports = [getShopProducts, getProductShopID, insertProductShop, updateShopProduct, deleteProductShop];
