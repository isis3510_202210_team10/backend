const mdbconn = require("../lib/utils/mongo");
const {ObjectId} = require('mongodb');
const env = require("dotenv").config();

function getOrders() {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("orders")
      .find({})
      .toArray();
  });
}

function getOrderID(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("orders")
      .findOne({"_id" : new ObjectId(id)});
  });
}

function insertOrder(order) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("orders")
      .insertOne(order); // Si no se provee un ID, este será generado automáticamente
  });
}

function updateOrder(id, data) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("orders")
      .updateOne(
        { _id: new ObjectId(id) }, // Filtro al documento que queremos modificar
        { $set: data } // El cambio que se quiere realizar
      );
  });
}

function deleteOrder(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("orders")
      .deleteOne({ _id: new ObjectId(id)});
  });
}

function getItemsFromOrderId(orderId) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("items")
      .find({"orderId": orderId})
      .toArray();
  });
}

module.exports = [getOrders, getOrderID, insertOrder, updateOrder, deleteOrder, getItemsFromOrderId];