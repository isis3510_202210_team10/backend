const mdbconn = require("../lib/utils/mongo");
const {ObjectId} = require('mongodb');
const env = require("dotenv").config();

// Correct de ID if it is not hexadecimal
function getRealID(id){
  if(id.length == 24 && !id.startsWith('ID')){
    return new ObjectId(id);
  }
  else{
    return id;
  }
}

function getShops() {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shops")
      .find({})
      .toArray();
  });
}

function getShopID(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shops")
      .findOne({"_id" : getRealID(id)});
  });
}

function getShopByEmail(email) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shops")
      .findOne({"email" : email});
  });
}

function insertShop(shop) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shops")
      .insertOne(shop); // Si no se provee un ID, este será generado automáticamente
  });
}

function updateShop(id, data) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shops")
      .updateOne(
        { _id: getRealID(id) }, // Filtro al documento que queremos modificar
        { $set: data } // El cambio que se quiere realizar
      );
  });
}

function deleteShop(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shops")
      .deleteOne({ _id: getRealID(id)});
  });
}

function getShops() {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shops")
      .find({})
      .toArray();
  });
}

function getOrdersFromShopId(shopId) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("orders")
      .find({"shopId": shopId})
      .toArray();
  });
}

function getProductsFromShopId(shopId) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("shopProducts")
      .find({"shopId": shopId})
      .toArray();
  });
}

function getAlertsFromShopId(shopId) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("alerts")
      .find({"userId": shopId})
      .toArray();
  });
}

module.exports = [getShops, getShopID, insertShop, updateShop, deleteShop, getOrdersFromShopId, getShopByEmail, getProductsFromShopId, getAlertsFromShopId];
