const mdbconn = require("../lib/utils/mongo");
const {ObjectId} = require('mongodb');
const env = require("dotenv").config();

// Correct de ID if it is not hexadecimal
function getRealID(id){
  if(id.length == 24 && !id.startsWith('ID')){
    return new ObjectId(id);
  }
  else{
    return id;
  }
}

// Get the suppliers registered in the database
function getSuppliers() {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("suppliers")
      .find({})
      .toArray();
  });
}

// Get one supplier registered in the database by his/her ID.
function getSupplierID(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("suppliers")
      .findOne({"_id" : getRealID(id)});
  });
}

// Get one supplier registered in the database by his/her email.
function getSupplierByEmail(email) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("suppliers")
      .findOne({"email" : email});
  });
}

// Inserting a new supplier to the database
function insertSupplier(supplier) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("suppliers")
      .insertOne(supplier); // Si no se provee un ID, este será generado automáticamente
  });
}

// Updating an existing supplier in the database
function updateSupplier(id, data) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("suppliers")
      .updateOne(
        { _id: new ObjectId(id) }, // Filtro al documento que queremos modificar
        { $set: data } // El cambio que se quiere realizar
      );
  });
}

// Delete an existing supplier in the database
function deleteSupplier(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("suppliers")
      .deleteOne({ _id: new ObjectId(id)});
  });
}

// Get all orders from supplier ID
function getOrdersFromSupplierId(supplierId) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("orders")
      .find({"supplierId": supplierId})
      .toArray();
  });
}

// Get all alerts from supplier ID
function getAlertsFromSupplierId(supplierId) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("alerts")
      .find({"userId": supplierId})
      .toArray();
  });
}


module.exports = [getSuppliers, getSupplierID, insertSupplier, 
  updateSupplier, deleteSupplier, getOrdersFromSupplierId, getSupplierByEmail, getAlertsFromSupplierId];