const mdbconn = require("../lib/utils/mongo");
const {ObjectId} = require('mongodb');
const env = require("dotenv").config();

function getItems() {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("items")
      .find({})
      .toArray();
  });
}

function getItemID(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("items")
      .findOne({"_id" : new ObjectId(id)});
  });
}

function insertItem(item) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("items")
      .insertOne(item); // Si no se provee un ID, este será generado automáticamente
  });
}

function updateItem(id, data) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("items")
      .updateOne(
        { _id: new ObjectId(id) }, // Filtro al documento que queremos modificar
        { $set: data } // El cambio que se quiere realizar
      );
  });
}

function deleteItem(id) {
  return mdbconn.conn().then((client) => {
    return client
      .db(process.env.DATABASE_NAME_MONGO_DB)
      .collection("items")
      .deleteOne({ _id: new ObjectId(id)});
  });
}

module.exports = [getItems, getItemID, insertItem, updateItem, deleteItem];
